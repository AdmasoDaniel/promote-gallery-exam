import React from 'react';
import './App.scss';
import Gallery from '../Gallery';
import FontAwesome from 'react-fontawesome';

class App extends React.Component {
  static propTypes = {};

  constructor() {
    super();
    this.state = {
      tag: 'art',
      titleOrDescription: null,
      displayWishList: false
    };
    this.timeout = null;
  }

  handleSearch(event) {
    let searchText = event.target.value;
    if (this.timeout) clearTimeout(this.timeout);
    this.timeout = setTimeout(() => {
      if (searchText == '')
        this.setState({ tag: 'art' });
      else
        this.setState({ tag: searchText });
    }, 500);
  }

  handleSearchByTitleOrDescription(event) {
    let searchText = event.target.value;
    if (searchText == '' && this.timeout) {
      clearTimeout(this.timeout);
      return;
    }
    if (this.timeout) clearTimeout(this.timeout);
    this.timeout = setTimeout(() => {
      this.setState({ titleOrDescription: searchText });
    }, 500);
  }

  render() {
    return (
      <div className='app-root'>
        <div className='app-header'>
          <h2>Flickr Gallery</h2>
          <div >
            <input className='app-input' placeholder='Search...' onChange={event => this.handleSearch(event)} />

            <input id='titleOrDescription' className='app-input' placeholder='Search By Title or Description'
              onChange={event => this.handleSearchByTitleOrDescription(event)} />

          </div>
          <FontAwesome className='image-icon' name='image' title='All Images'
            onClick={() => this.setState({ displayWishList: false })} />

          <FontAwesome className='image-icon' name='heart' title='My Wish List'
            onClick={() => this.setState({ displayWishList: true })} />
        </div>

        <Gallery
          tag={this.state.tag}
          titleOrDescription={this.state.titleOrDescription}
          displayWishList={this.state.displayWishList}
        />
      </div>
    );
  }
}

export default App;
