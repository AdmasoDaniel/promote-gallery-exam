import React from 'react';
import PropTypes from 'prop-types';
import FontAwesome from 'react-fontawesome';
import './Image.scss';

class Image extends React.Component {
  static propTypes = {
    dto: PropTypes.object,
    displayWishList: PropTypes.bool,
    getWishListImages: PropTypes.func,
    handleClone: PropTypes.func
  };

  constructor(props) {
    super(props);
    this.calcImageSize = this.calcImageSize.bind(this);
    this.state = {
      size: 220,
      filter: 'none',
      displayModal: false,
      displayRangeBar: false,
      range: 100,
      like: 'thumbs-down',
      likeTitle: 'like',
      imageBackground: 'rgba(0, 0, 0, 0.7)'
    };
  }

  calcImageSize() {
    const targetSize = 220;
    const imagesPerRow = Math.round(document.body.clientWidth / targetSize);
    const size = document.body.clientWidth / imagesPerRow;
    this.setState({ size: size });
  }

  componentDidMount() {
    this.calcImageSize();
    if (localStorage.getItem(this.props.dto.id)) {
      this.setState({ like: 'thumbs-up', likeTitle: 'unlike' });
    }
  }

  componentWillReceiveProps(props) {
    if (localStorage.getItem(props.dto.id)) {
      this.setState({ like: 'thumbs-up', likeTitle: 'unlike' });
    }
  }

  urlFromDto(dto) {
    return `https://farm${dto.farm}.staticflickr.com/${dto.server}/${dto.id}_${
      dto.secret
      }.jpg`;
  }

  handleFilter() {
    let filters = ['contrast', 'brightness', 'grayscale', 'invert', 'sepia'];

    let randomFilter = Math.floor(Math.random() * 5);

    this.setState({ filter: filters[randomFilter], displayRangeBar: true, imageBackground: 'none' });
    setTimeout(() => {
      this.setState({ displayRangeBar: false, imageBackground: 'rgba(0, 0, 0, 0.7)' })
    }, 10000)
  }

  handleLike(event) {
    if (event.target.title === 'like') {
      localStorage.setItem(this.props.dto.id, JSON.stringify(this.props.dto))
      this.setState({ like: 'thumbs-up', likeTitle: 'unlike' });
    } else {
      localStorage.removeItem(this.props.dto.id)
      if (this.props.displayWishList) {
        this.props.getWishListImages();
      }
      this.setState({ like: 'thumbs-down', likeTitle: 'like' });
    }
  }

  render() {
    let filter = '';
    if (this.state.filter == 'none') {
      filter = 'none'
    } else {
      filter = `${this.state.filter}(${this.state.range}%)`
    }

    return (
      <div
        className='image-root'
        style={{
          backgroundImage: `url(${this.urlFromDto(this.props.dto)})`,
          width: this.state.size + 'px',
          height: this.state.size + 'px',
          filter: filter
        }}
      >

        <div className='rangeModal' style={{ zIndex: 1, display: this.state.displayRangeBar ? 'block' : 'none' }}>
          <input type='range' name='range' value={this.state.range}
            onChange={(event) => this.setState({ range: event.target.value })} />
        </div>

        <div className='modal' style={{
          zIndex: 1,
          position: 'fixed',
          paddingTop: '150px',
          display: this.state.displayModal ? 'block' : 'none',
          background: this.state.imageBackground
        }}>

          <div className='modal-content' >
            <img src={this.urlFromDto(this.props.dto)} />
            <i
              className='fa fa-times'
              style={{
                fontSize: '70px',
                position: 'absolute',
                right: '20px',
                top: '10px',
                cursor: 'pointer'
              }}
              onClick={() => this.setState({ displayModal: false })}
            />
          </div>
        </div>

        <div style={{ background: this.state.imageBackground }}>
          <FontAwesome className='image-icon' name={this.state.like} title={this.state.likeTitle}
            onClick={(event) => this.handleLike(event)} />

          <FontAwesome className='image-icon' name='clone' title='clone'
            onClick={() => this.props.handleClone(this.props.dto)} />

          <FontAwesome className='image-icon' name='filter' title='filter'
            onClick={() => this.handleFilter()} />

          <FontAwesome className='image-icon' name='expand' title='expand'
            onClick={() => this.setState({ displayModal: true, filter: 'none' })} />
        </div>
      </div>
    );
  }
}

export default Image;
