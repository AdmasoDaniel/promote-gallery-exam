import React from 'react';
import PropTypes from 'prop-types';
import axios from 'axios';
import Image from '../Image';
import './Gallery.scss';
import apiKey from './Keys';
import preloader from '../../preloader.gif';


class Gallery extends React.Component {
  static propTypes = {
    tag: PropTypes.string,
    titleOrDescription: PropTypes.string,
    displayWishList: PropTypes.bool
  };

  constructor(props) {
    super(props);
    this.state = {
      images: [],
      tag: 'art',
      titleOrDescription: null,
      preloaderDisplay: false,
      displayWishList: false
    };
    this.pages = 1;
  }

  getImages(tag, pages, isNewSearch) {
    this.setState({ preloaderDisplay: true })
    const getImagesUrl = `services/rest/?method=flickr.photos.search&api_key=${apiKey}&tags=${tag}&tag_mode=any&per_page=37&page=${pages}&format=json&safe_search=1&nojsoncallback=1`;
    const baseUrl = 'https://api.flickr.com/';
    axios({
      url: getImagesUrl,
      baseURL: baseUrl,
      method: 'GET'
    })
      .then(res => res.data)
      .then(res => {
        if (
          res &&
          res.photos &&
          res.photos.photo &&
          res.photos.photo.length > 0
        ) {
          if (isNewSearch) {
            this.setState({
              images: res.photos.photo,
              tag: tag,
              preloaderDisplay: false,
              displayWishList: false
            });
          } else {
            this.setState({
              images: [...this.state.images, ...res.photos.photo],
              tag: tag,
              preloaderDisplay: false,
              displayWishList: false
            });
          }
        } else { //if there is no results.
          this.setState({
            images: [],
            tag: tag,
            preloaderDisplay: false,
            displayWishList: false
          });
        }
      });
  }

  filterByTitleOrDescription(titleOrDescription) {
    this.setState({ preloaderDisplay: true })
    let tempArr = [];
    let imagesLength = this.state.images.length;
    let numberOfLoops = 0;

    this.state.images.map(image => {
      axios
        .get(
          `https://www.flickr.com/services/rest/?method=flickr.photos.getInfo&api_key=${apiKey}&photo_id=${
          image.id}&format=json&nojsoncallback=1`)
        .then(result => {
          numberOfLoops++;
          if (
            (result &&
              result.data &&
              result.data.photo &&
              (result.data.photo.description._content.indexOf(titleOrDescription) > -1) ||
              result.data.photo.title._content.indexOf(titleOrDescription) > -1)
          ) {
            tempArr.push(image);
          }
          if (numberOfLoops == imagesLength) {
            this.setState({
              images: tempArr,
              titleOrDescription: titleOrDescription,
              preloaderDisplay: false
            });
          }
        });
    });
  }

  getWishListImages() {
    let wishListImages = []
    let keys = Object.keys(localStorage)
    for (let i = 0; i < keys.length; i++) {
      wishListImages.push(JSON.parse(localStorage.getItem(keys[i])));
    }
    this.setState({ images: wishListImages, displayWishList: true });
  }

  componentDidMount() {
    window.addEventListener('scroll', this.handleScroll);
    this.getImages(this.props.tag, 1, true);
  }

  componentWillReceiveProps(props) {
    if (props.tag != this.state.tag) {
      window.addEventListener('scroll', this.handleScroll);
      this.getImages(props.tag, 1, true);
    }
    else if (props.titleOrDescription != this.state.titleOrDescription) {
      if (this.state.images.length == 0) return;
      window.addEventListener('scroll', this.handleScroll);
      this.filterByTitleOrDescription(props.titleOrDescription);
    }
    else if (props.displayWishList) {
      window.removeEventListener('scroll', this.handleScroll);
      this.getWishListImages()
    }
    else {
      window.addEventListener('scroll', this.handleScroll);
      this.getImages(props.tag, 1, true);
    }
  }

  handleScroll = () => {
    if ((Math.ceil(document.documentElement.scrollTop) + document.documentElement.clientHeight) === document.documentElement.scrollHeight) {
      this.getImages(this.props.tag, ++this.pages, false);
    }
  };


  handleClone(image) {
    let tempImages = [...this.state.images]
    tempImages.push(image);
    this.setState({ images: tempImages });
  }

  render() {
    let noResults = false;
    let emptyWishList = false

    if (this.state.images.length == 0 && !this.state.preloaderDisplay) {
      if (this.state.displayWishList) {
        emptyWishList = true;
      } else {
        noResults = true;
      }
    }
    return (
      <div className='gallery-root'>
        <img src={preloader} style={{ margin: '150px auto', display: this.state.preloaderDisplay ? 'block' : 'none' }} />
        <h1 style={{ margin: '150px auto', display: noResults ? 'block' : 'none' }}>There is no result for this search.</h1>
        <h1 style={{ margin: '150px auto', display: emptyWishList ? 'block' : 'none' }}>Your wish list is empty!</h1>
        <div className='row'>
          {this.state.images.map((dto, index) => {
            return (

              <Image key={'image-' + index} dto={dto}
                handleClone={image => this.handleClone(image)}
                displayWishList={this.state.displayWishList}
                getWishListImages={() => this.getWishListImages()}
              />
            );
          })}
        </div>
      </div>
    );
  }
}

export default Gallery;
